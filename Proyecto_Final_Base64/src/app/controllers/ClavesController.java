package app.controllers;

import java.security.InvalidKeyException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import app.configuracion.FxmlPaths;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class ClavesController extends AppController {

	@FXML
	private Button btGenerar;

	@FXML
	private Button btVolver;

	@FXML
	private TextField tfNombre;

	@FXML
	void generarCertificado(ActionEvent event) {
		try {
			if (tfNombre.getText().isBlank()) {
				mostrarAviso("El nombre del certificado no puede estar vacio");
			} else {
				service.addCertificado(usuario.getUsuario(),
						serviceDecode.generarCerificado(usuario, tfNombre.getText()));
			}
		} catch (InvalidKeySpecException | InvalidKeyException | NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException e) {
			e.printStackTrace();
		}
		MenuController controller = (MenuController) cambiarVista(FxmlPaths.FXML_MENU);
		controller.listaCertificados();
	}

	@FXML
	void volverALista(ActionEvent event) {
		MenuController controller = (MenuController) cambiarVista(FxmlPaths.FXML_MENU);
		controller.listaCertificados();
	}

	@FXML
	public void initialize() {
		btGenerar.setId("boton");
		btVolver.setId("boton");
	}

	@FXML
	void key(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			generarCertificado(null);

		}
	}
}
