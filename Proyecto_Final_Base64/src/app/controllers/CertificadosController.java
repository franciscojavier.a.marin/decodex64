package app.controllers;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import app.configuracion.FxmlPaths;
import app.exception.LoginException;
import app.modelo.Certificado;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.DialogPane;
import javafx.stage.StageStyle;
import javafx.scene.control.Alert.AlertType;

public class CertificadosController extends AppController {
	public static final ObservableList<String> data = FXCollections.observableArrayList();
	private static final Logger log = LogManager.getLogger(CertificadosController.class.getName());

	@FXML
	private Button btEliminar;

	@FXML
	private Button btNuevo;

	@FXML
	private Button btDecode;

	@FXML
	private ListView<String> certificados;

	@FXML
	void crearCertificado(ActionEvent event) {
		MenuController controller = (MenuController) cambiarVista(FxmlPaths.FXML_MENU);
		controller.crearCertificado(null);

	}

	@FXML
	void eliminarCertificado(ActionEvent event) {
		Alert pregunta = new Alert(AlertType.CONFIRMATION);
		pregunta.setContentText("¿esta seguro de que quiere borrar?");
		pregunta.setTitle("Confirmacion");
		pregunta.setHeaderText(null);
		pregunta.getDialogPane().setPrefHeight(170);
		pregunta.getDialogPane().setPrefWidth(280);
		pregunta.initStyle(StageStyle.UNDECORATED);
		DialogPane dialogPane = pregunta.getDialogPane();
		dialogPane.getStylesheets().add(getClass().getResource("/styles/styles.css").toExternalForm());
		dialogPane.getStyleClass().add("myDialog");
		Optional<ButtonType> resultado = pregunta.showAndWait();
		if (resultado.get() == ButtonType.OK) {
			log.log(Level.DEBUG, "borrando certificado...");
			Certificado certificado = null;
			List<Certificado> certificados = usuario.getCertificados();
			for (int i = 0; i < certificados.size(); i++) {
				if (certificados.get(i).getNombre().equals(this.certificados.getSelectionModel().getSelectedItem())) {
					certificado = certificados.get(i);
					service.borrarCertificado(certificado, usuario.getUsuario());
				}
			}
			try {
				usuario = service.logIn(usuario.getUsuario(), usuario.getContraseña());
			} catch (LoginException e) {
				e.printStackTrace();
			}
			cambiarDatos(usuario.getCertificados());

		}
	}

	@FXML
	void irADecodificador(ActionEvent event) {
		MenuController controller = (MenuController) cambiarVista(FxmlPaths.FXML_MENU);
		controller.decode(null);
	}

	private void cambiarDatos(List<Certificado> certificados) {
		data.clear();
		for (int i = 0; i < certificados.size(); i++) {
			data.add(certificados.get(i).getNombre());
		}
	}

	public void initialize() {
		cambiarDatos(usuario.getCertificados());
		this.certificados.setItems(data);
		btEliminar.setId("boton");
		btNuevo.setId("boton");
		btDecode.setId("boton");
	}
}
