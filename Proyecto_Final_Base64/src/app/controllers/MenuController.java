package app.controllers;

import app.configuracion.FxmlPaths;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.layout.BorderPane;

public class MenuController extends AppController {

	@FXML
	private BorderPane panel;

	public void listaCertificados() {
		panel.setCenter(cargarVista(FxmlPaths.FXML_LISTA_CERTIFICADO));
	}

	@FXML
	public void cerrarSesion(ActionEvent event) {
		cambiarVista(FxmlPaths.FXML_LOGIN);
	}

	@FXML
	public void crearCertificado(ActionEvent event) {
		panel.setCenter(cargarVista(FxmlPaths.FXML_CERTIFICADO));
	}

	@FXML
	public void decode(ActionEvent event) {
		panel.setCenter(cargarVista(FxmlPaths.FXML_DECODE));
	}

	@FXML
	public void salir(ActionEvent event) {
		salirApp();
	}

}
