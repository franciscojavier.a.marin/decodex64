package app.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import app.App;
import app.modelo.Usuario;
import app.service.DecodeService;
import app.service.MongoService;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class AppController {

	private static Stage primaryStage;
	private static final Logger log = LogManager.getLogger(AppController.class.getName());
	public static MongoService service;
	public static DecodeService serviceDecode;
	public static Usuario usuario;

	public AppController() {
	}

	public AppController(Stage Stage, MongoService service, DecodeService service1) {
		primaryStage = Stage;
		AppController.service = service;
		AppController.serviceDecode = service1;
	}

	public AppController cambiarVista(String fxml) {
		try {
			URL url = App.class.getResource(fxml);
			FXMLLoader loader = new FXMLLoader(url);
			Scene scene = new Scene(loader.load());
			primaryStage.setScene(scene);
			AppController controllerDestino = loader.getController();
			log.log(Level.DEBUG, "Abriendo vista: " + fxml);
			return controllerDestino;
		} catch (IOException e) {
			log.log(Level.ERROR, "No se ha podido abrir el fxml con ruta " + fxml, e);
			throw new RuntimeException("No se ha podido cargar fxml con ruta " + fxml, e);
		}
	}

	public Parent cargarVista(String fxml) {
		try {
			URL url = App.class.getResource(fxml);
			FXMLLoader loader = new FXMLLoader(url);
			log.log(Level.DEBUG, "Cargando vista: " + fxml);
			return loader.load();
		} catch (IOException e) {
			log.log(Level.ERROR, "No se ha podido cargar el fxml con ruta " + fxml, e);
			throw new RuntimeException("No se ha podido cargar fxml con ruta " + fxml, e);
		}
	}

	public void mostrarError(String mensaje) {
		Alert error = new Alert(AlertType.ERROR);
		error.setContentText(mensaje);
		error.setTitle("Error");
		error.setHeaderText(null);
		error.getDialogPane().setPrefHeight(170);
		error.getDialogPane().setPrefWidth(280);
		error.initStyle(StageStyle.UNDECORATED);
		DialogPane dialogPane = error.getDialogPane();
		dialogPane.getStylesheets().add(getClass().getResource("/styles/styles.css").toExternalForm());
		dialogPane.getStyleClass().add("myDialog");
		error.showAndWait();
	}

	public void mostrarAviso(String mensaje) {
		Alert error = new Alert(AlertType.INFORMATION);
		error.setContentText(mensaje);
		error.setTitle("Advertencia");
		error.setHeaderText(null);
		error.getDialogPane().setPrefHeight(170);
		error.getDialogPane().setPrefWidth(280);
		error.initStyle(StageStyle.UNDECORATED);
		DialogPane dialogPane = error.getDialogPane();
		dialogPane.getStylesheets().add(getClass().getResource("/styles/styles.css").toExternalForm());
		dialogPane.getStyleClass().add("myDialog");
		error.showAndWait();
	}

	public Boolean salirApp() {
		Alert pregunta = new Alert(AlertType.CONFIRMATION);
		pregunta.setContentText("¿esta seguro de que quiere salir?");
		pregunta.setTitle("Confirmacion");
		pregunta.setHeaderText(null);
		pregunta.getDialogPane().setPrefHeight(170);
		pregunta.getDialogPane().setPrefWidth(280);
		pregunta.initStyle(StageStyle.UNDECORATED);
		DialogPane dialogPane = pregunta.getDialogPane();
		dialogPane.getStylesheets().add(getClass().getResource("/styles/styles.css").toExternalForm());
		dialogPane.getStyleClass().add("myDialog");
		Optional<ButtonType> resultado = pregunta.showAndWait();
		if (resultado.get() == ButtonType.OK) {
			log.log(Level.DEBUG, "Saliendo...");
			System.exit(0);
		} else if (resultado.get() == ButtonType.CANCEL) {
			return true;
		}
		return null;
	}

}
