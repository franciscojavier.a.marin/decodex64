package app.controllers;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import app.modelo.Certificado;
import app.modelo.CertificadoRSA;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;

public class DecodeController extends AppController {
	private static final Logger log = LogManager.getLogger(DecodeController.class.getName());
	@FXML
	private TextArea base64;

	@FXML
	private ComboBox<String> certificados;

	@FXML
	private TextArea texto;

	@FXML
	private Button btCodificar;

	@FXML
	private Button btDecodificar;

	@FXML
	void codificar(ActionEvent event) {
		if (certificados.getValue() == null) {
			mostrarAviso("debe seleccionar un certificado");
		} else {

			if (texto.getText().isBlank()) {
				mostrarAviso("No ha escrito nada, primero debe escribir el mensaje que desee codificar");
			} else {
				log.log(Level.DEBUG, "Cifrando con PublicKey...");
				String nomCerti = certificados.getValue();
				CertificadoRSA cert = new CertificadoRSA();
				Certificado c = null;
				List<Certificado> ce = usuario.getCertificados();
				for (int i = 0; i < ce.size(); i++) {
					if (ce.get(i).getNombre().equals(nomCerti)) {
						c = ce.get(i);
					}
				}
				cert.setCertificado(c);
				try {
					base64.setText(serviceDecode.cifradoConClave(cert.getSymetricKey(), texto.getText()));
				} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
						| IllegalBlockSizeException | BadPaddingException | InvalidKeySpecException e) {
					e.printStackTrace();
					log.log(Level.ERROR, "Ha habido un error al cifrar con PublicKey; ", e);
					mostrarError("Se ha producido un error al cifrar el mensaje");
				}
				texto.setText("");
				log.log(Level.DEBUG, "Cifrado correctamente");
			}

		}
	}

	@FXML
	void decodificar(ActionEvent event) {
		if (certificados.getValue() == null) {
			mostrarAviso("debe seleccionar un certificado");
		} else {

			if (base64.getText().isBlank()) {
				mostrarAviso("No ha escrito nada, primero debe escribir el mensaje en Base64 que desee decodificar");
			} else {

				log.log(Level.DEBUG, "Descifrando con PrivateKey...");
				String nomCerti = certificados.getValue();
				CertificadoRSA cert = new CertificadoRSA();
				Certificado c = null;
				List<Certificado> ce = usuario.getCertificados();
				for (int i = 0; i < ce.size(); i++) {
					if (ce.get(i).getNombre() == nomCerti) {
						c = ce.get(i);
					}
				}
				cert.setCertificado(c);
				try {
					texto.setText(serviceDecode.descifrarConClave(cert.getSymetricKey(), base64.getText()));
				} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
						| IllegalBlockSizeException | BadPaddingException | InvalidKeySpecException e) {
					e.printStackTrace();
					log.log(Level.ERROR, "Ha habido un error al descifrar con PrivateKey; ", e);
					mostrarError("Se ha producido un error al descifrar el mensaje");
				}
				base64.setText("");
				log.log(Level.DEBUG, "Descifrado correctamente");
			}
		}
	}

	@FXML
	public void initialize() {
		List<String> listaCert = new ArrayList<>();
		for (Certificado cert : usuario.getCertificados()) {
			listaCert.add(cert.getNombre());
		}
		ObservableList<String> items = FXCollections.observableArrayList(listaCert);
		certificados.setItems(items);
		btCodificar.setId("boton");
		btDecodificar.setId("boton");
	}

}
