package app.controllers;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import app.configuracion.FxmlPaths;
import app.exception.LoginException;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class RegisterController extends AppController {
	private static final Logger log = LogManager.getLogger(RegisterController.class.getName());
	@FXML
	private Button btRegistrar;

	@FXML
	private Button btVolver;

	@FXML
	public TextField tfUsuario;
	@FXML
	public PasswordField pfPass;

	@FXML
	public void registrar() {
		try {
			service.logIn(tfUsuario.getText(), pfPass.getText());
			log.log(Level.DEBUG, "Error al registrar: El usuario ya existe");
			mostrarAviso("El usuario ya existe");
		} catch (LoginException e) {
			if (pfPass.getText().length() < 8) {
				log.log(Level.DEBUG, "Error al registrar: La contraseña debe de ser almenos de 8 caracteres");
				mostrarAviso("La contraseña debe de ser almenos de 8 caracteres");
			} else {
				log.log(Level.DEBUG, "registrando...");
				usuario = service.crear(tfUsuario.getText(), pfPass.getText());
				MenuController controller = (MenuController) cambiarVista(FxmlPaths.FXML_MENU);
				controller.listaCertificados();
			}
		}
	}

	@FXML
	public void salir() {
		cambiarVista(FxmlPaths.FXML_LOGIN);
	}

	@FXML
	public void initialize() {
		btVolver.setId("boton");
		btRegistrar.setId("boton");
	}

	@FXML
	void key(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (event.getSource().equals(tfUsuario)) {
				pfPass.requestFocus();
			}
			if (event.getSource().equals(pfPass)) {
				registrar();
			}
		}
		if (event.getCode() == KeyCode.UP) {
			if (event.getSource().equals(pfPass)) {
				tfUsuario.requestFocus();
			}
		}
		if (event.getCode() == KeyCode.DOWN) {
			if (event.getSource().equals(tfUsuario)) {
				pfPass.requestFocus();
			}
		}
	}
}
