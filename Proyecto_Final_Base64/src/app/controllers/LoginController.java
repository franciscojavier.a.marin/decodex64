package app.controllers;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import app.configuracion.FxmlPaths;
import app.exception.LoginException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.DialogPane;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.StageStyle;

public class LoginController extends AppController {
	private static final Logger log = LogManager.getLogger(LoginController.class.getName());
	@FXML
	private Button btEntrar;

	@FXML
	private Button btRegistrar;

	@FXML
	private Button btSalir;

	@FXML
	private TextField tfUser;

	@FXML
	private PasswordField pfPasword;

	public LoginController() {
	}

	@FXML
	void entrar(ActionEvent event) {
		log.log(Level.DEBUG, "Iniciando Sesion...");
		try {
			if (!tfUser.getText().isBlank() && !pfPasword.getText().isBlank()) {
				usuario = service.logIn(tfUser.getText(), pfPasword.getText());
				MenuController controller = (MenuController) cambiarVista(FxmlPaths.FXML_MENU);
				controller.listaCertificados();
			} else {
				mostrarMensaje("El usuario y/o la contraseña no pueden estar vacios");
				btEntrar.getStylesheets().clear();
				btEntrar.getStylesheets().add("/styles/boton.css");
			}
		} catch (LoginException e) {
			mostrarError(e.getMessage());
			log.log(Level.DEBUG, "La contraseña no coincide");
			if (e.getMessage().equals("La contraseña no coincide")) {
				pfPasword.setText("");
			} else {
				tfUser.setText("");
				pfPasword.setText("");
			}
		}

	}

	private void mostrarMensaje(String mensaje) {
		Alert pregunta = new Alert(AlertType.WARNING);
		pregunta.setContentText(mensaje);
		pregunta.setTitle("Alerta");
		pregunta.setHeaderText(null);
		pregunta.getDialogPane().setPrefHeight(170);
		pregunta.getDialogPane().setPrefWidth(280);
		pregunta.initStyle(StageStyle.UNDECORATED);
		DialogPane dialogPane = pregunta.getDialogPane();
		dialogPane.getStylesheets().add(getClass().getResource("/styles/styles.css").toExternalForm());
		dialogPane.getStyleClass().add("myDialog");
		pregunta.showAndWait();
	}

	public void registrar() {
		cambiarVista(FxmlPaths.FXML_REGISTRAR);
	}

	@FXML
	void key(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (event.getSource().equals(tfUser)) {
				pfPasword.requestFocus();
			}
			if (event.getSource().equals(pfPasword)) {
				entrar(null);
			}
		}
		if (event.getCode() == KeyCode.UP) {
			if (event.getSource().equals(pfPasword)) {
				tfUser.requestFocus();
			}
		}
		if (event.getCode() == KeyCode.DOWN) {
			if (event.getSource().equals(tfUser)) {
				pfPasword.requestFocus();
			}
		}
	}

	@FXML
	void salir(ActionEvent event) {
		if (salirApp()) {
		}
	}

	@FXML
	public void initialize() {
		btEntrar.setId("boton");
		btRegistrar.setId("boton");
		btSalir.setId("boton");
	}

}