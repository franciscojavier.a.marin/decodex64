package app;

public class Launcher {
	public static void main(String[] args) {
		System.setProperty("log4j.configurationFile", "config/log4j.properties");
		App.main(args);
	}
}
