package app.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.conversions.Bson;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;

import app.configuracion.Config;
import app.configuracion.MongoUtil;
import app.exception.LoginException;
import app.modelo.Certificado;
import app.modelo.Usuario;

public class MongoService {
	private static final Logger log = LogManager.getLogger(MongoService.class.getName());
	public Config config;

	public MongoService(Config config) {
		this.config = config;
	}

	public Usuario logIn(String usuario, String contraseña) throws LoginException {
		Usuario user = null;
		MongoDatabase db = conectarAMongo();
		MongoCollection<Usuario> us = db.getCollection("Usuario", Usuario.class);
		Bson filtro = Filters.eq("usuario", usuario);
		FindIterable<Usuario> it = us.find(filtro);
		log.log(Level.DEBUG, "Iniciando Sesion");
		user = it.first();
		if (user == null) {
			throw new LoginException("El usuario no coincide");
		}
		if (user != null) {
			String contraseñaCod = DigestUtils.sha512Hex(contraseña);
			if (!user.getContraseña().equals(contraseñaCod)) {
				throw new LoginException("La contraseña no coincide");
			}
		}
		return user;
	}

	public void addCertificado(String nombre, Certificado cert) {
		MongoDatabase db = conectarAMongo();
		MongoCollection<Usuario> us = db.getCollection("Usuario", Usuario.class);
		Bson filtro = Filters.eq("usuario", nombre);
		Bson añadir = Updates.push("certificados", cert);
		us.updateOne(filtro, añadir);
	}

	public Usuario crear(String usuario, String contraseña) {
		MongoDatabase db = conectarAMongo();
		MongoCollection<Usuario> us = db.getCollection("Usuario", Usuario.class);
		Usuario u = new Usuario();
		u.setUsuario(usuario);
		String contraseñaCod = DigestUtils.sha512Hex(contraseña);
		u.setContraseña(contraseñaCod);
		List<Certificado> cert = new ArrayList<>();
		u.setCertificados(cert);
		us.insertOne(u);
		return u;
	}

	public void borrarCertificado(Certificado certificado, String nombre) {
		MongoDatabase db = conectarAMongo();
		MongoCollection<Usuario> us = db.getCollection("Usuario", Usuario.class);
		Bson filtro = Filters.eq("usuario", nombre);
		Bson delete = Updates.pull("certificados", certificado);
		us.updateOne(filtro, delete);
	}

	public MongoDatabase conectarAMongo() {
		return new MongoUtil(config).getDatabase(config.getPropiertieString("DBName"));

	}
}
