package app.service;

import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import app.configuracion.Config;
import app.modelo.Certificado;
import app.modelo.Usuario;

public class DecodeService {
	private static final Logger log = LogManager.getLogger(DecodeService.class.getName());
	public Config config;

	public DecodeService() {
	}

	public DecodeService(Config config) {
		this.config = config;
	}

	public Certificado generarCerificado(Usuario usuario, String nombreCertificado) throws InvalidKeySpecException,
			InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {

		KeyPairGenerator generadorRSA;
		Certificado certificado = null;
		try {
			log.log(Level.DEBUG, "Generando Certificado...");
			generadorRSA = KeyPairGenerator.getInstance("RSA");
			generadorRSA.initialize(1024);
			KeyPair keyPair = generadorRSA.generateKeyPair();
			PublicKey publicKey = keyPair.getPublic();
			PrivateKey privateKey = keyPair.getPrivate();
			KeyGenerator claveSimetrica = KeyGenerator.getInstance("AES");
			claveSimetrica.init(128);
			SecretKey simetrica = claveSimetrica.generateKey();
			byte[] claveSimetricaCodificada = cifrarClave(publicKey, simetrica.getEncoded());
			certificado = new Certificado();
			certificado.setNombre(nombreCertificado);
			certificado.setClavePrivada(cifrarBase64(privateKey.getEncoded()));
			certificado.setClavePublica(cifrarBase64(publicKey.getEncoded()));
			certificado.setClaveSimetrica(cifrarBase64(claveSimetricaCodificada));
			log.log(Level.DEBUG, "Certificado creado");
			usuario.añadirCertificado(certificado);
		} catch (NoSuchAlgorithmException e) {
			log.log(Level.ERROR, "Ha habido un error al generar el certificado: ", e);
		}
		return certificado;
	}

	public String cifradoConClave(PublicKey publicKey, String mensaje) throws NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Cipher cifradorRSA = Cipher.getInstance("RSA");
		cifradorRSA.init(Cipher.ENCRYPT_MODE, publicKey);
		return cifrarBase64(cifradorRSA.doFinal(mensaje.getBytes()));
	}

	public String cifradoConClave(SecretKey sk, String mensaje) throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Cipher cifradorAES = Cipher.getInstance("AES");
		cifradorAES.init(Cipher.ENCRYPT_MODE, sk);
		return cifrarBase64(cifradorAES.doFinal(mensaje.getBytes()));
	}

	public byte[] descifrarConClave(PrivateKey privateKey, String mensaje) throws NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Cipher cifradorRSA = Cipher.getInstance("RSA");
		cifradorRSA.init(Cipher.DECRYPT_MODE, privateKey);
		return cifradorRSA.doFinal(descifrarBase64byte(mensaje.getBytes()));
	}

	public String descifrarConClave(SecretKey sk, String mensaje) throws NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Cipher cifradorAES = Cipher.getInstance("AES");
		cifradorAES.init(Cipher.DECRYPT_MODE, sk);
		return new String(cifradorAES.doFinal(descifrarBase64byte(mensaje.getBytes())));
	}

	public String cifrarBase64(byte[] a) {
		return Base64.getEncoder().encodeToString(a);
	}

	public byte[] descifrarBase64byte(byte[] a) {
		return Base64.getDecoder().decode(a);
	}

	private byte[] cifrarClave(PublicKey pk, byte[] encodeSK) throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		Cipher cifradorRSA = Cipher.getInstance("RSA");
		cifradorRSA.init(Cipher.ENCRYPT_MODE, pk);
		return cifradorRSA.doFinal(encodeSK);
	}

}
