package app.configuracion;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {
	private Properties config;

	public Config() {
		config = new Properties();
		InputStream is = getClass().getResourceAsStream("/config/app-config.propierties");
		try {
			config.load(is);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public String getPropiertieString(String p) {
		return config.getProperty(p);
	}

	public Boolean getPropiertieBoolean(String p) {
		return Boolean.valueOf(config.getProperty(p));
	}

	public Integer getPropiertieInteger(String p) {
		return Integer.parseInt(config.getProperty(p));
	}
}
