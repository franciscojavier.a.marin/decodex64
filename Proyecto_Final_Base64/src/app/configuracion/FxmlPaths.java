package app.configuracion;

public class FxmlPaths {

	public static final String FXML_BASIC = "/fxml/";
	public static final String FXML_DECODE =  FXML_BASIC + "decode.fxml";
	public static final String FXML_LOGIN =  FXML_BASIC + "login.fxml";
	public static final String FXML_CERTIFICADO =  FXML_BASIC + "generarClaves.fxml";
	public static final String FXML_LISTA_CERTIFICADO =  FXML_BASIC + "certificados.fxml";
	public static final String FXML_MENU =  FXML_BASIC + "menu.fxml";
	public static final String FXML_REGISTRAR =  FXML_BASIC + "registrar.fxml";
}
