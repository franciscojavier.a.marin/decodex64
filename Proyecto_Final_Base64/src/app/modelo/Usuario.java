package app.modelo;

import java.util.List;

public class Usuario {
	private String usuario;
	private String contraseña;
	private List<Certificado> certificados;

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getContraseña() {
		return contraseña;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}

	public List<Certificado> getCertificados() {
		return certificados;
	}

	public void setCertificados(List<Certificado> certificados) {
		this.certificados = certificados;
	}

	public void añadirCertificado(Certificado certificado) {
		certificados.add(certificado);
	}
}
