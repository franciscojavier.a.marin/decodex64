package app.modelo;

import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import app.service.DecodeService;

public class CertificadoRSA {
	private Certificado certificado;
	private DecodeService service;

	public CertificadoRSA() {
		service = new DecodeService();
	}

	public void setCertificado(Certificado certificado) {
		this.certificado = certificado;
	}

	public PrivateKey getPrivateKey() throws InvalidKeySpecException, NoSuchAlgorithmException {
		PKCS8EncodedKeySpec ks = new PKCS8EncodedKeySpec(service.descifrarBase64byte(certificado.getClavePrivada().getBytes()));
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		return keyFactory.generatePrivate(ks);
	}

	public PublicKey getPublicKey() throws InvalidKeySpecException, NoSuchAlgorithmException {
		X509EncodedKeySpec ks = new X509EncodedKeySpec(service.descifrarBase64byte(certificado.getClavePublica().getBytes()));
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		return keyFactory.generatePublic(ks);
	}

	public SecretKey getSymetricKey() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException {
		byte[] claveSimetrica = service.descifrarConClave(getPrivateKey(), certificado.getClaveSimetrica());
		return new SecretKeySpec(claveSimetrica, 0, claveSimetrica.length, "AES");
	}
}
