package app.modelo;

public class Certificado {
	private String nombre;
	private String clavePrivada;
	private String clavePublica;
	private String claveSimetrica;

	public Certificado() {
	}

	public String getClavePrivada() {
		return clavePrivada;
	}
	
	public void setClavePrivada(String clavePrivada) {	
		this.clavePrivada = clavePrivada;
	}

	public String getClavePublica() {
		return clavePublica;
	}

	public void setClavePublica(String clavePublica) {	
		this.clavePublica = clavePublica;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getClaveSimetrica() {
		return claveSimetrica;
	}

	public void setClaveSimetrica(String claveSimetrica) {
		this.claveSimetrica = claveSimetrica;
	}

}
