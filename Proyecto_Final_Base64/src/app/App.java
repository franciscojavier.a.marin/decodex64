package app;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import app.configuracion.Config;
import app.configuracion.FxmlPaths;
import app.controllers.AppController;
import app.service.DecodeService;
import app.service.MongoService;
import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class App extends Application {
	private static final Logger log = LogManager.getLogger(App.class.getName());
	
	private static String fuente = "/styles/Pixellari.ttf";
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		Font.loadFont(App.class.getResource(fuente).toExternalForm(), 10);
		Config config = new Config();
		MongoService service = new MongoService(config);
		DecodeService service1 = new DecodeService(config);
		AppController controler = new AppController(primaryStage, service, service1);
		controler.cambiarVista(FxmlPaths.FXML_LOGIN);
		primaryStage.initStyle(StageStyle.UNDECORATED);
		primaryStage.getIcons().add(new Image("/images/icono-app.png"));
		primaryStage.show();
		log.log(Level.DEBUG, "Iniciando la aplicacion");
	}

	public static void main(String[] args) {
		launch();
	}

}
